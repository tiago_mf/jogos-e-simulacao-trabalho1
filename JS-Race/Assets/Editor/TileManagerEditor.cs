﻿using UnityEngine;
using UnityEditor;

public class TileManagerEditor : MonoBehaviour
{
    [MenuItem("Assets/Create/TileManager")]
    public static void CreateTileManagerAsset()
    {
        TileManager asset = ScriptableObject.CreateInstance<TileManager>();

        AssetDatabase.CreateAsset(asset, "Assets/tileManager.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
