﻿using UnityEngine;

public class Vehicle : MonoBehaviour, IControl
{
    public VehicleProperties properties;

    public VehicleSystemControl systemControl;
    public VehicleSystemWheel systemWheel;
    
    public void SetupVehicle(int id)
    {
        properties.Setup(this, id);
        systemControl.Setup(this);
        systemWheel.Setup();
    }

    void FixedUpdate()
    {
        systemControl.FixedUpdate();
        systemWheel.FixedUpdate();
    }

    public void SimulateController(PlayerInput playerInput)
    {
        properties.UpdateInput(playerInput);
    }

    void OnTriggerEnter(Collider other)
    {
        Level.Instance.VehicleEndedRace(this);
    }
}
