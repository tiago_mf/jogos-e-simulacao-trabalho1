﻿using UnityEngine;

[System.Serializable]
public class VehicleSounds
{
    public AudioSource motorAudioSource;

    public AudioClip motorIdle;

    public void Setup(VehicleProperties properties)
    {
        PlayCarSound(0);
    }

    public void PlayCarSound(float Velocity)
    {
        motorAudioSource.Play();
    }
}
