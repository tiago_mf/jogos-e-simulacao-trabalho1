﻿using UnityEngine;

public class VehicleWheel : MonoBehaviour
{
    private WheelCollider wheelCollider;
    private Transform wheelVisual;

    private Vector3 wheelVisualEulerAngles;

    public bool clockwiseRotation;
    private int rotationAxis;

    void Awake()
    {
        wheelCollider = transform.GetComponent<WheelCollider>();
        wheelVisual = transform.GetChild(0);
        wheelVisualEulerAngles = wheelVisual.localEulerAngles;

        rotationAxis = clockwiseRotation ? -1 : 1;
    }

    public void PerformFixedUpdate()
    {
        UpdateWheelRotation();
        UpdateWheelPosition();
    }

    private void UpdateWheelPosition()
    {
        WheelHit hit;
        Vector3 futureWheelPosition;

        if(wheelCollider.GetGroundHit(out hit))
        {
            futureWheelPosition = GetWheelLocalPosition(hit);
        }
        else
        {
            futureWheelPosition = GetWheelLocalPosition();
        }

        wheelVisual.localPosition = Vector3.Lerp(wheelVisual.localPosition, futureWheelPosition, Time.deltaTime * 10);
        wheelVisual.localEulerAngles = wheelVisualEulerAngles + new Vector3(0, wheelCollider.steerAngle, 0);
    }
    
    private Vector3 GetWheelLocalPosition(WheelHit hit)
    {
        float y = transform.InverseTransformPoint(hit.point).y + wheelCollider.radius;
        return new Vector3(0, y, 0);
    }

    private Vector3 GetWheelLocalPosition()
    {
        return new Vector3(0, -wheelCollider.suspensionDistance, 0);
    }

    private void UpdateWheelRotation()
    {
        wheelVisualEulerAngles.x += wheelCollider.rpm;
        wheelVisualEulerAngles.x = wheelVisualEulerAngles.x % 360f;
    }

    public void AddTorque(float force)
    {
        wheelCollider.motorTorque = force;
    }

    public void Break(float force)
    {
        wheelCollider.brakeTorque = force;
    }

    public void SetDirection(float angle)
    {
        wheelCollider.steerAngle = Mathf.Lerp(wheelCollider.steerAngle, angle, Time.deltaTime * 5);
    }
}
