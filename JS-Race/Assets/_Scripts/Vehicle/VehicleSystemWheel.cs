﻿[System.Serializable]
public class VehicleSystemWheel
{
    public VehicleWheel wheelFrontLeft;
    public VehicleWheel wheelFrontRight;
    public VehicleWheel wheelBackLeft;
    public VehicleWheel wheelBackRights;

    public void Setup()
    {

    }

    public void FixedUpdate()
    {
        wheelFrontLeft.PerformFixedUpdate();
        wheelFrontRight.PerformFixedUpdate();
        wheelBackLeft.PerformFixedUpdate();
        wheelBackRights.PerformFixedUpdate();
    }

    public void WheelDirection(float horizontal)
    {
        wheelFrontLeft.SetDirection(horizontal);
        wheelFrontRight.SetDirection(horizontal);
    }

    public void WheelTorque(float force)
    {
        wheelFrontLeft.AddTorque(force);
        wheelFrontRight.AddTorque(force);
        wheelBackLeft.AddTorque(force);
        wheelBackRights.AddTorque(force);
    }

    public void WheelBreak(float force)
    {
        wheelFrontLeft.Break(force);
        wheelFrontRight.Break(force);
        wheelBackLeft.Break(force);
        wheelBackRights.Break(force);
    }
}
