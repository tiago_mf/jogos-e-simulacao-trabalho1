﻿using UnityEngine;

[System.Serializable]
public class VehicleProperties
{
    public float accelerationForce;
    
    public float horizontal { get; private set; }
    public float vertical { get; private set; }

    public Transform centerOfMass;
    public Rigidbody rigidBody { get; private set; }

    public Transform carBody;

    public Renderer renderer { get; private set; }
    public Material backLights { get; private set; }

    public int carId { get; private set; }
    public bool endedRace { get; set; }

    public VehicleSounds sounds;

    public void Setup(Vehicle vehicle, int id)
    {
        rigidBody = vehicle.GetComponent<Rigidbody>();
        rigidBody.centerOfMass = centerOfMass.localPosition;
        renderer = carBody.GetComponent<Renderer>();
        backLights = renderer.material;
        endedRace = false;
        carId = id;

        sounds.Setup(this);
    }

    public void UpdateInput(PlayerInput playerInput)
    {
        horizontal = playerInput.horizontal;
        vertical = playerInput.vertical;
    }
}
