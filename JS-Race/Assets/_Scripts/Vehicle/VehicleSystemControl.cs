﻿using UnityEngine;

[System.Serializable]
public class VehicleSystemControl
{
    private Vehicle vehicle;
    
    public void Setup(Vehicle vehicle)
    {
        this.vehicle = vehicle;
    }

    public void FixedUpdate()
    {
        WheelDirection();
        WheelTorque();
    }

    private void WheelDirection()
    {
        Rigidbody rig = vehicle.properties.rigidBody;

        float velocity = Mathf.Max(50, rig.velocity.magnitude) - 49;
        float direction = Mathf.Clamp(vehicle.properties.horizontal * 35 / velocity, -30, 30);

        vehicle.systemWheel.WheelDirection(direction);
    }

    private void WheelTorque()
    {
        UpdateLights();

        float force = vehicle.properties.vertical * vehicle.properties.accelerationForce;

        vehicle.systemWheel.WheelBreak(0);
        vehicle.systemWheel.WheelTorque(force);
    }

    private void UpdateLights()
    {
        Rigidbody rig = vehicle.properties.rigidBody;
        float velocity = rig.velocity.magnitude;

        Renderer renderer = vehicle.properties.renderer;
        Material backlights = vehicle.properties.backLights;

        if (vehicle.properties.vertical == 0)
        {
            backlights.SetColor("_EmissionColor", Color.white * 1f);
            DynamicGI.SetEmissive(renderer, Color.white * 1f);
        }
        else if (vehicle.properties.vertical < 0)
        {
            backlights.SetColor("_EmissionColor", Color.white * 4f);
            DynamicGI.SetEmissive(renderer, Color.white * 4);
        }
        else
        {
            backlights.SetColor("_EmissionColor", Color.white * 1f);
            DynamicGI.SetEmissive(renderer, Color.white * 1f);
        }

        DynamicGI.UpdateMaterials(renderer);
    }

    public void ResetVehicle()
    {
        vehicle.transform.position += Vector3.up;
        vehicle.transform.rotation = Quaternion.identity;
    }

    public void ResetVehicle(Vector3 position)
    {
        vehicle.transform.position = position;
        vehicle.transform.rotation = Quaternion.identity;
    }

    public void ResetVehicleWithDirection(Vector3 direction)
    {
        ResetVehicle();
        vehicle.transform.forward = direction;
    }

}
