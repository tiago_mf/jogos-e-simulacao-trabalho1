﻿using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform follow;
    private Vector3 lastPosition;

	void LateUpdate ()
    {
        FollowObject();
    }

    private void FollowObject()
    {
        if (follow == null) return;

        Vector3 futurePosition = follow.position - follow.forward * 2 + follow.up * 1.1f;
        transform.position = Vector3.Lerp(transform.position, futurePosition, Time.deltaTime * 4);

        Vector3 futureDirection = (follow.position + follow.forward - transform.position).normalized;
        transform.forward = Vector3.Lerp(transform.forward, futureDirection, Time.deltaTime * 4);
    }

    public void SetFollowObject(Transform followObject)
    {
        follow = followObject;
    }
}
