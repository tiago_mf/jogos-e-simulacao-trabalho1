﻿using UnityEngine;

public abstract class Player : MonoBehaviour
{
    public int playerId { get; private set; }
    protected Vehicle vehicle;
    protected PlayerInput playerInput;

    public virtual void Setup(int playerId)
    {
        this.playerId = playerId;
        playerInput = new PlayerInput();
    }

    void FixedUpdate()
    {
        SimulatePlayer();
    }

    protected virtual void SimulatePlayer()
    {
        UpdateInput();
        if (vehicle != null)
        {
            vehicle.SimulateController(playerInput);
        }
    }

    public virtual void GetControlOfVehicle(Vehicle vehicle)
    {
        this.vehicle = vehicle;
    }
    protected abstract void UpdateInput();
    public abstract bool IsPlayer();
}
