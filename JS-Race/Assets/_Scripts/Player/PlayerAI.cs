﻿using System;
using UnityEngine;

public class PlayerAI : Player
{
    private Track track;

    private Tile currentTile;
    private Transform currentControlPoint;
    private Vector3 carDirection;

    public override void Setup(int playerId)
    {
        base.Setup(playerId);
        track = Track.Instance;

        currentTile = track.GetBeginTile().nextTile;
        currentControlPoint = GetControlPoint(currentTile);
    }

    public override bool IsPlayer()
    {
        return false;
    }

    protected override void UpdateInput()
    {
        CheckIfCantMove();
        UpdateControlPoint();
        UpdateCarDirection();

        playerInput.horizontal = carDirection.x;
        playerInput.vertical = carDirection.z * 0.8f;
    }

    private void UpdateCarDirection()
    {
        if (currentControlPoint != null)
        {
            carDirection = currentControlPoint.position - vehicle.transform.position;
            carDirection.Normalize();
            carDirection = vehicle.transform.InverseTransformDirection(carDirection);

            Debug.DrawRay(vehicle.transform.position, vehicle.transform.TransformDirection(carDirection) * 10);
        }
    }

    private void CheckIfCantMove()
    {
        if (Vector3.Dot(vehicle.transform.up, -Vector3.up) > 0 ||
            vehicle.properties.rigidBody.velocity.magnitude < 0.1f)
        {
            ResetCar();
        }
    }

    private void ResetCar()
    {
        Vector3 resetDirection = carDirection;
        resetDirection.y = 0;
        resetDirection = vehicle.transform.InverseTransformDirection(resetDirection);

        vehicle.systemControl.ResetVehicleWithDirection(resetDirection);
    }

    private void UpdateControlPoint()
    {
        if (currentTile.nextTile == null) return;

        Tile nextTile = currentTile.nextTile;
        Vector3 vehiclePosition = vehicle.transform.position;

        if (Vector3.Distance(vehiclePosition, currentControlPoint.position) > 7 ) return;

        currentTile = nextTile;

        if (currentTile.controlPoints.Length == 0) return;

        currentControlPoint = GetControlPoint(currentTile);
    }

    private Transform GetControlPoint(Tile tile)
    {
        Transform[] controlPoints = tile.controlPoints;
        int pointId = UnityEngine.Random.Range(0, controlPoints.Length);

        return controlPoints[pointId];
    }
}
