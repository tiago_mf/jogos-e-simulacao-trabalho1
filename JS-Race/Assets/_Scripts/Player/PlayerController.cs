﻿using System;
using UnityEngine;

public class PlayerController : Player
{
    private PlayerCamera playerCamera;

    public override void Setup(int playerId)
    {
        base.Setup(playerId);
        InstantiateCamera();
    }
    public override bool IsPlayer()
    {
        return true;
    }

    protected override void SimulatePlayer()
    {
        base.SimulatePlayer();

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (vehicle != null)
            {
                vehicle.systemControl.ResetVehicle();
            }
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (vehicle != null)
            {
                vehicle.systemControl.ResetVehicle(new Vector3(5, 5, 5));
            }
        }
    }

    public override void GetControlOfVehicle(Vehicle vehicle)
    {
        base.GetControlOfVehicle(vehicle);
        playerCamera.SetFollowObject(vehicle.transform);
    }

    private void InstantiateCamera()
    {
        GameObject cameraPrefab = Resources.Load<GameObject>("PlayerCamera");
        playerCamera = GameObject.Instantiate(cameraPrefab).GetComponent<PlayerCamera>();
    }

    protected override void UpdateInput()
    {
        playerInput.horizontal = Input.GetAxisRaw("Horizontal");
        playerInput.vertical = Input.GetAxisRaw("Vertical");
    }
}
