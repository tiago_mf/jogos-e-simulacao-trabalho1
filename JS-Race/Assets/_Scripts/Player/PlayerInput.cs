﻿public class PlayerInput
{
    public float horizontal { get; set; }
    public float vertical { get; set; }

    public PlayerInput()
    {
        horizontal = 0;
        vertical = 0;
    }
}
