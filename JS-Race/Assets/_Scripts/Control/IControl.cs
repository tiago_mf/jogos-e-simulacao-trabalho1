﻿public interface IControl
{
    void SimulateController(PlayerInput playerInput);
}