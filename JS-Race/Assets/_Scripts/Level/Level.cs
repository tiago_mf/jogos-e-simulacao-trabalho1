﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public static Level Instance;

    [Range(1, 6)]
    public int numberOfPlayers;
    
    public GameObject uiWin;
    public GameObject uiLost;

    private bool oponentEndedFirst;
    private bool raceEnded;

    private TileManager tileManager;
    private Track track;

    private List<Vector3> startingPoints;
    private List<Player> players;

    private List<Vehicle> vehicles;

	void Start ()
    {
        DynamicGI.UpdateEnvironment();

        Instance = this;

        startingPoints = new List<Vector3>();
        players = new List<Player>();
        vehicles = new List<Vehicle>();

        tileManager = Resources.Load<TileManager>("TileManager");
        tileManager.SetupTiles();
        track = new Track(tileManager);

        CreateMap();
        CreatePlayers();
        CreateVehicles();

        PositionVehiclesOnStart();
        AssignVehiclesToPlayers();

        oponentEndedFirst = false;
        raceEnded = false;
    }

    private void CreateMap()
    {
        track.CreateNewTrack(Random.Range(10,50));

        Transform[] startingPositions = track.GetBeginTile().GetStartingPoints();
        for(int i = 0; i < startingPositions.Length; i++)
        {
            startingPoints.Add(startingPositions[i].position);
        }
    }

    private void CreatePlayers()
    {
        for(int i = 0; i < numberOfPlayers - 1; i++)
        {
            CreatePlayerAI(i);
        }

        CreatePlayer(numberOfPlayers-1);
    }

    private void CreatePlayerAI(int id)
    {
        GameObject newPlayer = new GameObject("Player");
        Player player = newPlayer.AddComponent<PlayerAI>();
        player.Setup(id);
        players.Add(player);
    }

    private void CreatePlayer(int id)
    {
        GameObject newPlayer = new GameObject("Player");
        Player player = newPlayer.AddComponent<PlayerController>();
        player.Setup(id);
        players.Add(player);
    }

    private void CreateVehicles()
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            InstantiateVehicle(i);
        }
    }

    private void InstantiateVehicle(int id)
    {
        GameObject vehiclePrefab = Resources.Load<GameObject>("Car_2");
        GameObject vehicleObject = GameObject.Instantiate(vehiclePrefab);

        Vehicle vehicle = vehicleObject.GetComponent<Vehicle>();
        vehicle.SetupVehicle(id);

        vehicles.Add(vehicle);
    }

    private void AssignVehiclesToPlayers()
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            players[i].GetControlOfVehicle(vehicles[i]);
        }
    }

    private void PositionVehiclesOnStart()
    {
        for(int i = 0; i < vehicles.Count; i++)
        {
            vehicles[i].transform.position = (startingPoints[i]);
        }
    }

    public void VehicleEndedRace(Vehicle vehicle)
    {
        if (vehicle.properties.endedRace) return;

        vehicle.properties.endedRace = true;
        int id = vehicle.properties.carId;

        if(players[id].IsPlayer())
        {
            EndRace();
        }
        else
        {
            oponentEndedFirst = true;
        }
    }

    public void EndRace()
    {
        if(raceEnded) return;
        raceEnded = true;

        if (oponentEndedFirst) uiLost.SetActive(true);
        else uiWin.SetActive(true);

        StartCoroutine(TimerToNewRace());
    }

    private IEnumerator TimerToNewRace()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
