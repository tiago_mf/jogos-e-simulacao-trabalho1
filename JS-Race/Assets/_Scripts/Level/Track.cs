﻿using System.Collections.Generic;
using UnityEngine;

public class Track
{
    public static Track Instance;

    private TileManager manager;

    private Tile_Begin begin;
    private Tile_End end;

    private int length;
    private Dictionary<Vector3, Tile> tiles;

    public Track(TileManager manager)
    {
        Instance = this;
        this.manager = manager;
        tiles = new Dictionary<Vector3, Tile>();
    }

    public Tile GetTileAtPosition(Vector3 position)
    {
        return tiles.ContainsKey(position) ? tiles[position] : null;
    }

    public Tile_Begin GetBeginTile()
    {
        return begin;
    }
    public Tile GetEndTile()
    {
        return end;
    }

    public void CreateNewTrack(int length)
    {
        this.length = length;
        //Set initial tile
        begin = Tile.Create(manager.GetTileBegin()) as Tile_Begin;
        
        Tile lastTile = begin;
        Tile currentTile = begin;
        for(int i = 0; i < length; i++)
        {
            //Set last tile and set it on the dictionary
            lastTile = currentTile;
            tiles.Add(lastTile.Position, lastTile);

            //Get next possible tiles based on last tile type and direction
            KeyValuePair<ETileType, EDirection>[] nextPossibleTiles = TileConstruction.GetNextTile(lastTile.type, lastTile.direction);
            
            //Get a random tile
            int rand = Random.Range(0, 6);
            KeyValuePair<ETileType, EDirection> nextTile = nextPossibleTiles[rand % nextPossibleTiles.Length];
            
            //Get the position of the next tile, and create it.
            Vector3 nextPosition = lastTile.GetNextTilePosition();
            currentTile = Tile.Create(manager.GetTile(nextTile.Key, rand), nextPosition, nextTile.Value);

            //Assign this tile to the last tile
            lastTile.SetNextTile(currentTile);
        }
        //Add last tile to the dictionary
        tiles.Add(currentTile.Position, currentTile);
        AddEnding(currentTile);
        CreateTrackEnvironment();
    }

    private void AddEnding(Tile currentTile)
    {
        Vector3 nextPosition = currentTile.GetNextTilePosition();

        end = Tile.Create(manager.GetTileEnd(), nextPosition, currentTile.direction) as Tile_End;

        currentTile.SetNextTile(end);

        //Point Ending at last Tile
        end.transform.LookAt(currentTile.transform);
        Vector3 rotation = end.transform.localEulerAngles;
        rotation.y -= 90;
        end.transform.localEulerAngles = rotation;

        tiles.Add(end.Position, end);
    }

    private void CreateTrackEnvironment()
    {
        Vector3[] positions = new Vector3[]
        {
            //Top left->right
            new Vector3(-Tile.TILESIZE, 0, Tile.TILESIZE),
            new Vector3(0, 0, Tile.TILESIZE),
            new Vector3(Tile.TILESIZE, 0, Tile.TILESIZE),
            //Middle left->right. No need to check for center. It's always taken
            new Vector3(-Tile.TILESIZE, 0, 0),
            new Vector3(Tile.TILESIZE, 0, 0),
            //Bottom left->right
            new Vector3(-Tile.TILESIZE, 0, -Tile.TILESIZE),
            new Vector3(0, 0, -Tile.TILESIZE),
            new Vector3(Tile.TILESIZE, 0, -Tile.TILESIZE),
        };
        Tile tile = begin;
        while(tile != null)
        {
            for(int i = 0; i < positions.Length; i++)
            {
                Vector3 environmentPosition = tile.Position + positions[i];
                if (IsPositionTaken(environmentPosition)) continue;

                int randomTile = Random.Range(0, 10);
                Tile EnvironmentTile = Tile.Create(manager.GetTile(ETileType.Empty, randomTile), environmentPosition);
                int randomRotation = Random.Range(0, 4);
                randomRotation *= 90;
                EnvironmentTile.transform.localEulerAngles.Set(0, randomRotation, 0);
                tiles.Add(environmentPosition, EnvironmentTile);
            }
            tile = tile.nextTile; 
        }
    }

    private bool IsPositionTaken(Vector3 position)
    {
        return tiles.ContainsKey(position);
    }

    private KeyValuePair<ETileType, EDirection> GetNextTile(KeyValuePair<ETileType, EDirection>[] tiles, int index)
    {
        return tiles[index % tiles.Length];
    }
    
    
}
