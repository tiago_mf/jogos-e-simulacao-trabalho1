﻿using UnityEngine;

public class Tile_Begin : Tile
{
    private Transform spawnpoints;

    private Transform[] startingPoints;

    public override void Setup(Vector3 position, ETileType type, EDirection direction)
    {
        base.Setup(position, type, direction);  

        spawnpoints = transform.FindChild("SpawnPoints");

        startingPoints = new Transform[spawnpoints.childCount];

        for(int i = 0; i < spawnpoints.childCount; i++)
        {
            startingPoints[i] = spawnpoints.GetChild(i);
        }
    }

    public Transform[] GetStartingPoints()
    {
        return startingPoints;
    }
}
