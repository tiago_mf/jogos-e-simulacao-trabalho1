﻿using UnityEngine;

public class TileManager : ScriptableObject
{
    [Header("Track Ends")]
    public TilePrefab begin;
    public TilePrefab end;

    [Header("Outside Road")]
    public TilePrefab[] empty;

    [Header("Straight Road")]
    public TilePrefab[] horizontal;
    public TilePrefab[] vertical;

    [Header("Curved Road")]
    public TilePrefab[] bottomLeft;
    public TilePrefab[] bottomRight;
    public TilePrefab[] leftTop;
    public TilePrefab[] rightTop;

    public void SetupTiles()
    {
        begin.SetType(ETileType.Vertical);
        //end.SetType(ETileType.Vertical);

        SetupTileArray(empty, ETileType.Empty);

        SetupTileArray(horizontal, ETileType.Horizontal);
        SetupTileArray(vertical, ETileType.Vertical);

        SetupTileArray(bottomLeft, ETileType.BottomLeft);
        SetupTileArray(leftTop, ETileType.LeftTop);
        SetupTileArray(bottomRight, ETileType.BottomRight);
        SetupTileArray(rightTop, ETileType.RightTop);
    }

    private void SetupTileArray(TilePrefab[] tiles, ETileType type)
    {
        for(int i = 0; i < tiles.Length; i++)
        {
            tiles[i].SetType(type);
        }
    }
    
    public TilePrefab GetTile(ETileType type, int index = 0)
    {
        switch(type)
        {
            case ETileType.Empty:
                return GetTile(empty, index);
            case ETileType.Horizontal:
                return GetTile(horizontal, index);
            case ETileType.Vertical:
                return GetTile(vertical, index);
            case ETileType.RightTop:
                return GetTile(rightTop, index);
            case ETileType.BottomRight:
                return GetTile(bottomRight, index);
            case ETileType.LeftTop:
                return GetTile(leftTop, index);
            case ETileType.BottomLeft:
                return GetTile(bottomLeft, index);
            default:
                return empty[0];
        }
    }

    private TilePrefab GetTile(TilePrefab[] tile, int index)
    {
        return tile[Mathf.CeilToInt(index % tile.Length)];
    }

    public TilePrefab GetTileBegin()
    {
        return begin;
    }

    public TilePrefab GetTileEnd()
    {
        return end;
    }
}
