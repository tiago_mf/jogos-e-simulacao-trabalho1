﻿using UnityEngine;

[System.Serializable]
public class TilePrefab
{
    public GameObject tilePrefab;
    private ETileType type = ETileType.Empty;
    
    public void SetType(ETileType type)
    {
        this.type = type;
    }

    public ETileType Type
    {
        get { return type; }
    }
}
