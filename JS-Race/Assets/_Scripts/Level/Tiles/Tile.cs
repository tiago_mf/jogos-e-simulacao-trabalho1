﻿using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public const int TILESIZE = 20;
    private static int numberTiles = 0;

    public ETileType type { get; private set; }
    public EDirection direction { get; private set; }

    public Tile nextTile { get; private set; }
    
    public Transform[] controlPoints { get; private set; }

    public virtual void Setup(Vector3 position, ETileType type, EDirection direction)
    {
        this.type = type;
        this.direction = direction;
        transform.position = position;

        Transform controlPointsParent = transform.FindChild("ControlPoints");

        controlPoints = new Transform[controlPointsParent.childCount];
        for(int i = 0; i < controlPoints.Length; i++)
        {
            controlPoints[i] = controlPointsParent.GetChild(i);
        }

        numberTiles++;
    }

    public Vector3 Position
    {
        get { return transform.position; }
    }

    public Vector3 GetNextTilePosition()
    {
        return transform.position + TileConstruction.GetNextTilePosition(direction);
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public void SetNextTile(Tile nextTile)
    {
        this.nextTile = nextTile;
    }


    public static Tile Create(TilePrefab tilePrefab)
    {
        return Create(tilePrefab, Vector3.zero);
    }

    public static Tile Create(TilePrefab tilePrefab, Vector3 position)
    {
        return Create(tilePrefab, position, EDirection.Top);
    }

    public static Tile Create(TilePrefab tilePrefab, Vector3 position, EDirection direction)
    {
        Tile newTile = GameObject.Instantiate<GameObject>(tilePrefab.tilePrefab).GetComponent<Tile>();
        newTile.Setup(position, tilePrefab.Type, direction);
        return newTile;
    }
}
