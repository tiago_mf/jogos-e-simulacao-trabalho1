﻿using System.Collections.Generic;
using UnityEngine;

public static class TileConstruction
{
    private static KeyValuePair<ETileType, EDirection>[] toTop = new KeyValuePair<ETileType, EDirection>[]
    {
        new KeyValuePair<ETileType, EDirection>(ETileType.BottomLeft, EDirection.Left),
        new KeyValuePair<ETileType, EDirection>(ETileType.BottomRight, EDirection.Right),
        new KeyValuePair<ETileType, EDirection>(ETileType.Vertical, EDirection.Top)
    };

    private static KeyValuePair<ETileType, EDirection>[] toRight = new KeyValuePair<ETileType, EDirection>[]
    {
        new KeyValuePair<ETileType, EDirection>(ETileType.LeftTop, EDirection.Top),
        new KeyValuePair<ETileType, EDirection>(ETileType.Horizontal, EDirection.Right)
    };
    private static KeyValuePair<ETileType, EDirection>[] toLeft = new KeyValuePair<ETileType, EDirection>[]
    {
        new KeyValuePair<ETileType, EDirection>(ETileType.RightTop, EDirection.Top),
        new KeyValuePair<ETileType, EDirection>(ETileType.Horizontal, EDirection.Left)
    };
    
    public static KeyValuePair<ETileType, EDirection>[] GetNextTile(ETileType type, EDirection direction)
    {
        switch (type)
        {
            case ETileType.Horizontal:
                return direction == EDirection.Right ? toRight : toLeft;
            case ETileType.Vertical:
                return toTop;
            case ETileType.BottomLeft:
                return toLeft;
            case ETileType.LeftTop:
                return direction == EDirection.Left ? toLeft : toTop;
            case ETileType.BottomRight:
                return toRight;
            case ETileType.RightTop:
                return direction == EDirection.Right ? toRight : toTop;
            default:
                return null;
        }
    }

    public static Vector3 GetNextTilePosition(EDirection direction)
    {
        switch(direction)
        {
            case EDirection.Top:
                return Vector3.forward * Tile.TILESIZE;
            case EDirection.Bottom:
                return -Vector3.forward * Tile.TILESIZE;
            case EDirection.Left:
                return -Vector3.right * Tile.TILESIZE;
            case EDirection.Right:
                return Vector3.right * Tile.TILESIZE;
        }

        return Vector3.zero;
    }
}
