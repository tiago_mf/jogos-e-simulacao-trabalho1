﻿public enum ETileType : int
{
    Empty = 0,
    Horizontal = 1,
    Vertical = 2,
    LeftTop = 3,
    BottomLeft = 4,
    RightTop = 5,
    BottomRight = 6
}
